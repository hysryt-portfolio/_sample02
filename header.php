<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="robots" content="noindex">
    <title><?php wp_title('|', true, 'right'); ?><?php bloginfo('name'); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo get_stylesheet_uri(); ?>">
    <script defer src="<?= get_template_directory_uri(); ?>/js/ofi.min.js"></script>
    <script defer src="<?= get_template_directory_uri(); ?>/js/script.js"></script>
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?>>
    <header class="header">
      <div class="header-top">
        <div class="inner">
          <?php
            $tag = "div";
            if ( is_front_page() ) {
              $tag = "h1";
            }
          ?>
          <<?= $tag; ?> class="header-top__description"><?php bloginfo('description'); ?></<?= $tag; ?>>
          <div class="header-top__main">
            <div class="header-brand">
              <a href="<?php bloginfo('url'); ?>"><?php bloginfo('name'); ?></a>
            </div>
            <div class="header-other">
              <div class="header-sns">
                <a href="https://ja-jp.facebook.com/" class="header-sns__item"><img src="<?= get_template_directory_uri(); ?>/img/common/icon_facebook.svg" /></a>
                <a href="https://twitter.com/" class="header-sns__item"><img src="<?= get_template_directory_uri(); ?>/img/common/icon_twitter.svg" /></a>
                <a href="https://www.instagram.com/" class="header-sns__item"><img src="<?= get_template_directory_uri(); ?>/img/common/icon_instagram.svg" /></a>
              </div>
              <form method="GET" action="<?php bloginfo('url'); ?>" class="header-search">
                <input type="text" name="s" class="header-search__text-form" placeholder="検索">
                <input type="submit" class="header-search__submit">
              </form>
            </div>
            <div class="header-hamburger-button _hidden" id="hamburgerButton">
              <span class="header-hamburger-button__bar"></span>
            </div>
          </div>
        </div>
      </div>
      <nav class="header-nav _hidden" id="hamburgerMenu">
        <div class="inner">
          <ul class="header-nav__list">
            <li class="header-nav-listitem">
              <a href="<?php bloginfo( 'url' ) ?>/about">
                <div class="header-nav-listitem__inner">
                  <p class="header-nav-listitem__title">ABOUT</p>
                  <p class="header-nav-listitem__subtitle">当ブログについて</p>
                </div>
              </a>
            </li>
            <li class="header-nav-listitem _has-dropdown">
              <div class="header-nav-listitem__inner">
                <p class="header-nav-listitem__title">CATEGORY</p>
                <p class="header-nav-listitem__subtitle">カテゴリ</p>
              </div>
              <div class="header-nav-listitem__dropdown">
                <ul class="header-nav-category-list">
                  <?php
                    $categories = get_categories();
                  ?>
                  <?php foreach( $categories as $category ) : ?>
                    <li class="header-nav-category-list__item">
                      <a href="<?= esc_attr ( get_category_link( $category->cat_ID ) ); ?>"><?= esc_html( $category->name ); ?></a>
                    </li>
                  <?php endforeach ?>
                </ul>
              </div>
            </li>
            <li class="header-nav-listitem _has-dropdown">
              <div class="header-nav-listitem__inner">
                <p class="header-nav-listitem__title">TAG</p>
                <p class="header-nav-listitem__subtitle">タグ</p>
              </div>
              <div class="header-nav-listitem__dropdown _taglist">
                <ul class="header-nav-tag-list">
                  <?php
                    $tags = get_tags( [
                      'orderby' => 'count',
                      'order' => 'DESC',
                    ] );
                  ?>
                  <?php foreach ( $tags as $tag ) : ?>
                    <li class="header-nav-tag-list__item">
                      <a href="<?= esc_html( get_tag_link( $tag->term_id ) ); ?>"><?= esc_html( $tag->name ); ?></a>
                    </li>
                  <?php endforeach ?>
                </ul>
              </div>
            </li>
            <li class="header-nav-listitem _has-dropdown">
              <div class="header-nav-listitem__inner">
                <p class="header-nav-listitem__title">ARCHIVE</p>
                <p class="header-nav-listitem__subtitle">アーカイブ</p>
              </div>
              <div class="header-nav-listitem__dropdown">
                <ul class="header-nav-archive-list">
                  <?php
                    $months = fetch_archive_months();
                  ?>
                  <?php foreach ( $months as $month ) : ?>
                    <li class="header-nav-archive-list__item">
                      <a href="<?= esc_attr( get_month_link( $month["year"], $month["month"] ) ); ?>"><?= esc_html( get_month_text( $month["year"], $month["month"] ) ); ?></a>
                    </li>
                  <?php endforeach ?>
                </ul>
              </div>
            </li>
            <li class="header-nav-listitem">
              <a href="<?php bloginfo( 'url' ) ?>/contact">
                <div class="header-nav-listitem__inner">
                  <p class="header-nav-listitem__title">CONTACT</p>
                  <p class="header-nav-listitem__subtitle">お問い合わせ</p>
                </div>
              </a>
            </li>
          </ul>
        </div>
      </nav>
    </header>
