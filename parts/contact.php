<header class="page-main__header">
  <h1 class="page-main__title">お問い合わせ</h1>
</header>
<div class="page-main__main">
　 <?= do_shortcode( '[contact-form-7 id="60" title="お問い合わせ" html_class="contact-form"]' ); ?>
</div>