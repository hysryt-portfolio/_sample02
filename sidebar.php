
<section class="sidebar-section">
  <h2 class="sidebar-section__title">最新記事</h2>
  <div class="sidebar-section__body _no-padding">
    <ul class="sidebar-article-list">
      <?php
        $posts = query_posts([
          'posts_per_page'   => 5,
        ]);
      ?>
      <?php foreach ( $posts as $post ) : the_post(); ?>
        <li class="sidebar-article-list-item">
          <a href="<?php the_permalink(); ?>">
            <div class="sidebar-article-list-item__image">
              <img src="<?php the_post_thumbnail_url( 'eyecatch_small' ); ?>" alt="">
            </div>
            <h3 class="sidebar-article-list-item__title"><?php the_title(); ?></h3>
          </a>
        </li>
      <?php endforeach ?>
      <?php
        wp_reset_query();
      ?>
    </ul>
  </div>
</section>
<section class="sidebar-section">
  <h2 class="sidebar-section__title">カテゴリ</h2>
  <div class="sidebar-section__body">
    <ul class="sidebar-category-list">
      <?php
        $categories = get_categories();
      ?>
      <?php foreach ( $categories as $category ) : ?>
        <li class="sidebar-category-list__item">
          <a href="<?= esc_attr( get_category_link( $category->cat_ID ) ); ?>"><?= esc_html( "$category->name($category->count)" ); ?></a>
        </li>
      <?php endforeach ?>
    </ul>
  </div>
</section>
<section class="sidebar-section">
  <h2 class="sidebar-section__title">タグ</h2>
  <div class="sidebar-section__body">
    <ul class="sidebar-tag-list">
      <?php
        $tags = get_tags( [
          'orderby' => 'count',
          'order' => 'DESC',
        ] );
      ?>
      <?php foreach ( $tags as $tag ) : ?>
        <li class="sidebar-tag-list__item">
          <a href="<?= esc_attr( get_tag_link( $tag->term_id ) ); ?>"><?= esc_html( $tag->name ); ?></a>
        </li>
      <?php endforeach ?>
    </ul>
  </div>
</section>