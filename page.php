<?php get_header(); ?>

<main>
  <?php if ( have_posts() ) : the_post(); ?>
    <div class="common-breadcrumbs">
      <ul class="inner common-breadcrumbs__list">
        <li class="common-breadcrumbs__list-item"><a href="<?php bloginfo('url'); ?>">TOP</a></li>
        <li class="common-breadcrumbs__list-item"><?= the_title(); ?></li>
      </ul>
    </div>
    <div class="inner">
      <div class="common-subpage-wrapper">
        <div class="common-subpage-wrapper__main">
          <article class="page-main">
            <?php
              $slug = get_post()->post_name;
              get_template_part( "parts/$slug" );
            ?>
          </article>
        </div>
        <div class="common-subpage-wrapper__side">
          <?php get_sidebar(); ?>
        </div>
      </div>
    </div>
  <?php endif ?>
</main>

<?php get_footer(); ?>