<header class="page-main__header">
  <h1 class="page-main__title">当ブログについて</h1>
</header>
<div class="page-main__main">
  <section class="page-main__section">
    <h2 class="page-main__section-title">説明</h2>
    <p>当ブログは複数人のライターが自由に投稿する形式のブログです。</p>
  </section>

  <section class="page-main__section">
    <h2 class="page-main__section-title">お問い合わせ</h2>
    <p>当ブログに対するご意見、お問い合わせは<a href="<?php bloginfo( 'url' ); ?>/contact">お問い合わせページ</a>から可能です。お気軽にご利用ください。</p>
    </section>
</div>