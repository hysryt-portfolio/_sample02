<?php get_header(); ?>

<main>
  <?php if ( have_posts() ) : the_post(); ?>
    <div class="article-eyecatch">
      <img src="<?php the_post_thumbnail_url( 'eyecatch_large' ); ?>" alt="">
    </div>
    <div class="common-breadcrumbs">
      <ul class="inner common-breadcrumbs__list">
        <li class="common-breadcrumbs__list-item"><a href="<?php bloginfo('url'); ?>">TOP</a></li>
        <li class="common-breadcrumbs__list-item"><a href="<?= esc_attr( get_category_posts_url() ); ?>"><?= esc_html( get_category_name() ); ?></a></li>
        <li class="common-breadcrumbs__list-item"><?= the_title(); ?></li>
      </ul>
    </div>
    <div class="inner">
      <div class="common-subpage-wrapper">
        <div class="common-subpage-wrapper__main">
          <article class="article">
            <div class="article-main">
              <header class="article-main__header">
                <h1 class="article-main__title"><?= the_title(); ?></h1>
                <ul class="article-main__labels">
                  <li class="article-main__category"><a href="<?= esc_attr( get_category_posts_url() ); ?>" class="common-category-label _<?= esc_attr( get_category_slug() ); ?>"><?= esc_html( get_category_name() ); ?></a></li>
                  <?php
                    $tags = get_the_tags();
                  ?>
                  <?php foreach ( $tags as $tag ) : ?>
                    <li class="article-main__tag"><a href="<?= esc_attr ( get_tag_link( $tag->term_id ) ); ?>"><?= esc_html( $tag->name ); ?></a></li>
                  <?php endforeach ?>
                </ul>
                <div class="article-main__info">
                  <p class="article-main__create-date"><?= the_time( 'Y/m/d' ); ?></p>
                  <?php if ( get_the_time( 'Ymd' ) < get_the_modified_time( 'Ymd' ) ) : ?>
                    <p class="article-main__update-date"><?= the_modified_time( 'Y/m/d' ); ?></p>
                  <?php endif ?>
                  <?php if ( get_comments_number() > 0 ) : ?>
                    <a href="#comments">
                      <p class="article-main__comment-num"><?= esc_html( get_comments_number() ); ?></p>
                    </a>
                  <?php endif ?>
                </div>
              </header>
              <div class="article-main__body article-content">
                <?php the_content(); ?>
              </div>
              <footer class="article-main__footer">
                <aside class="article-aside-sns">
                  <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-show-count="false">Tweet</a>
                  <div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button" data-size="small" data-mobile-iframe="true"><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;src=sdkpreparse" class="fb-xfbml-parse-ignore">シェア</a></div>
                  <div class="g-plus" data-action="share"></div>
                  <a href="http://b.hatena.ne.jp/entry/" class="hatena-bookmark-button" data-hatena-bookmark-layout="basic-label-counter" data-hatena-bookmark-lang="ja" title="このエントリーをはてなブックマークに追加"><img src="https://b.st-hatena.com/images/entry-button/button-only@2x.png" alt="このエントリーをはてなブックマークに追加" width="20" height="20" style="border: none;" /></a>
                  <a data-pocket-label="pocket" data-pocket-count="none" class="pocket-btn" data-lang="en"></a>
                </aside>
                <aside class="article-aside-related">
                  <h2 class="article-aside-related__title">関連記事</h2>
                  <ul class="article-aside-related__list">
                    <?php
                      $posts = query_related_posts();
                    ?>
                    <?php while ( have_posts() ) : the_post(); ?>
                      <li class="article-aside-related__list-item">
                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                      </li>
                    <?php endwhile ?>
                    <?php
                      wp_reset_query();
                    ?>
                  </ul>
                </aside>
                <aside class="article-aside-author">
                  <h2 class="archive-author__title">著者プロフィール</h2>
                  <div class="archive-author__image">
                    <img src="<?= esc_attr( get_wp_user_avatar_src( get_the_author_meta('ID'), 150 ) ); ?>" alt="">
                  </div>
                  <div class="archive-author__info">
                    <div class="archive-author__header">
                      <p class="archive-author__name"><?= esc_html( get_the_author() ); ?></p>
                      <ul class="archive-author__sns-list">
                        <?php if ( trim ( get_the_author_meta( 'facebook' ) ) != "" ) : ?>
                          <li class="archive-author__sns-list-item">
                            <a href="<?= esc_attr( get_the_author_meta( 'facebook' ) ); ?>"><img src="<?= get_template_directory_uri() ?>/img/common/icon_facebook.svg" alt="facebook"></a>
                          </li>
                        <?php endif ?>
                        <?php if ( trim ( get_the_author_meta( 'twitter' ) ) != "" ) : ?>
                          <li class="archive-author__sns-list-item">
                            <a href="<?= esc_attr( get_the_author_meta( 'twitter' ) ); ?>"><img src="<?= get_template_directory_uri() ?>/img/common/icon_twitter.svg" alt="twitter"></a>
                          </li>
                        <?php endif ?>
                        <?php if ( trim ( get_the_author_meta( 'instagram' ) ) != "" ) : ?>
                          <li class="archive-author__sns-list-item">
                            <a href="<?= esc_attr( get_the_author_meta( 'instagram' ) ); ?>"><img src="<?= get_template_directory_uri() ?>/img/common/icon_instagram.svg" alt="instagram"></a>
                          </li>
                        <?php endif ?>
                      </ul>
                    </div>
                    <p class="archive-author__description">
                      <?= esc_html( get_the_author_meta( 'description' ) ); ?>
                    </p>
                  </div>
                </aside>
              </footer>
            </div>
            <aside class="article-pager">
              <div class="article-pager__item _new">
                <?php
                  $new_post = get_next_post();
                ?>
                <?php if ( ! empty( $new_post ) ) : ?>
                  <a href="<?= esc_attr( get_permalink( $new_post->ID ) ); ?>">New</a>
                <?php endif ?>
              </div>
              <div class="article-pager__item _old">
                <?php
                  $old_post = get_previous_post();
                ?>
                <?php if ( ! empty( $old_post ) ) : ?>
                  <a href="<?= esc_attr( get_permalink( $old_post->ID ) ); ?>">Old</a>
                <?php endif ?>
              </div>
            </aside>
            <div class="article-comment-area">
              <section class="article-comments">
                <a id="comments" class="article-comments__anchor">
                <h2 class="article-comments__title">コメント</h2>
                <?php
                  $comments = get_comments( [
                    'post_id' => get_the_ID(),
                    'order' => 'ASC',
                  ] );
                ?>
                <?php foreach ( $comments as $comment ) : ?>
                  <article class="article-comment">
                    <a id="comment-<?= esc_attr( $comment->comment_ID ); ?>" class="article-comment__anchor">
                    <div class="article-comment__body">
                      <?php if ( $comment->comment_approved === '1' ) : ?>
                        <?= wpautop( esc_html( $comment->comment_content ) ); ?>
                      <?php else : ?>
                        <p class="article-comment__hold">このコメントは承認待ちです。</p>
                      <?php endif ?>
                    </div>
                    <footer class="article-comment__footer">
                      <p class="article-comment__time"><?= esc_html( $comment->comment_date ); ?></p>
                      <p class="article-comment__name"><?= esc_html( $comment->comment_author ); ?></p>
                      <?php if ( $comment->user_id != 0 ) : ?>
                        <p class="article-comment__avator">
                          <img src="<?= esc_attr( get_wp_user_avatar_src( $comment->user_id ), "30" ); ?>" alt="">
                        </p>
                      <?php endif ?>
                    </footer>
                  </article>
                <?php endforeach ?>
              </section>
              <section class="article-comment-submit">
                <h2 class="article-comment-submit__title">コメントの投稿</h2>
                <?php comment_form(); ?>
              </section>
            </div>
          </article>
        </div>
        <div class="common-subpage-wrapper__side">
          <?php get_sidebar(); ?>
        </div>
      </div>
    </div>
  <?php endif ?>
</main>

<?php get_footer(); ?>