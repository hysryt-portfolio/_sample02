<?php 

/**
 * コメント内容入力フィールドの表示位置を一番下に変更
 */
add_filter( 'comment_form_fields', function( $fields ) {
    unset( $fields['comment'] );
    $fields['comment'] = '<div class="article-comment-submit__row"><p class="article-comment-submit__label">コメント*</p><div class="article-comment-submit__field"><textarea name="comment"></textarea></div></div>';
 
    return $fields;
});

/**
 * コメントフォームのカスタマイズ
 */
add_filter( 'comment_form_defaults', function( $defaults ) {
    $commenter = wp_get_current_commenter();
    $user = wp_get_current_user();
    $user_identity = $user->exists() ? $user->display_name : '';

    $defaults['title_reply'] = '';
    $defaults['class_form'] = 'article-comment-submit__form';
    $defaults['logged_in_as'] = '<p class="article-comment-submit__message">' . sprintf('<a href="%1$s">%2$s</a>としてログイン中。<a href="%3$s">ログアウトしますか ?</a>', admin_url( 'profile.php' ), $user_identity, wp_logout_url( apply_filters( 'the_permalink', get_permalink( ) ) ) ) . '</span></p>';
    $defaults['comment_notes_before'] = '<p class="article-comment-submit__message">メールアドレスが公開されることはありません。</span></p>';
    $defaults['fields']['author'] = '<div class="article-comment-submit__row"><p class="article-comment-submit__label">お名前*</p><div class="article-comment-submit__field"><input type="text" name="author" value="' . esc_attr( $commenter['comment_author'] ) . '"></div></div>';
    $defaults['fields']['email'] = '<div class="article-comment-submit__row"><p class="article-comment-submit__label">メールアドレス*</p><div class="article-comment-submit__field"><input type="email" name="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '"></div></div>';
    $defaults['submit_field'] = '<p class="article-comment-submit__submit">%1$s %2$s</p>';

    if ( isset( $defaults['fields']['url'] ) ) {
        unset( $defaults['fields']['url'] );
    }

    return $defaults;
});


add_action( 'init', function() {
    add_theme_support( 'post-thumbnails' );
    add_image_size( 'eyecatch_large', 1680, 400, true );
    add_image_size( 'eyecatch_medium',  560, 350, true);
    add_image_size( 'eyecatch_small', 60, 38, true);
});


function get_category_name() {
    $category = get_the_category()[0];
    return $category->cat_name;
}


function get_category_slug() {
    $category = get_the_category()[0];
    return $category->category_nicename;
}


function get_category_posts_url() {
    $category = get_the_category()[0];
    return get_category_link( $category->cat_ID );
}


/**
 * ユーザーのプロフィールにSNS各種を追加
 */
add_filter( 'user_contactmethods', function( $methods ) {
    $new_methods = array_merge( $methods, [
        'facebook' => 'Facebook',
        'twitter' => 'Twitter',
        'instagram' => 'Instagram',
    ] );

    return $new_methods;
});


/**
 * ビジュアルエディタのカスタマイズ
 */
add_filter( 'tiny_mce_before_init', function( $settings ) {
    $settings['block_formats'] = '段落=p;見出し(h2)=h2;見出し(h3)=h3;見出し(h4)=h4;見出し(h5)=h5;見出し(h6)=h6;';

    return $settings;
});


/**
 * 関連記事を取得
 * 内部で query_posts を使用するため、ループから抜ける際に wp_reset_query を呼ぶ必要がある。
 */
function query_related_posts() {
    $posts = query_posts([
        'posts_per_page'   => 5,
        'cat' => get_the_category()[0]->cat_ID,
        'post__not_in' => [ get_the_ID() ],
    ]);

    return $posts;
}


/**
 * 投稿が存在する年月を取得する
 * 
 * @return (array) 'year'  年
 *                 'month' 月
 */
function fetch_archive_months() {
    global $wpdb;

    $query = "SELECT YEAR(post_date) AS `year`, MONTH(post_date) AS `month` "
           . "FROM $wpdb->posts "
           . "WHERE post_type = 'post' AND post_status = 'publish' "
           . "GROUP BY YEAR(post_date), MONTH(post_date) "
           . "ORDER BY post_date ";
    
    $last_changed = wp_cache_get_last_changed( 'posts' );
    $key = md5( $query );
    $key = "wp_get_archives:$key:$last_changed";

    if ( ! $results = wp_cache_get( $key, 'posts' ) ) {
        $results = $wpdb->get_results( $query );
        wp_cache_set( $key, $results, 'posts' );
    }

    $months = [];
    foreach ( $results as $result ) {
        $months[] = [
            'year' => $result->year,
            'month' => $result->month,
        ];
    }

    return $months;
}


function get_month_text( $year, $month ) {
    return sprintf('%04d/%02d', $year, $month);
}