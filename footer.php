    <footer class="footer">
      <div class="inner">
        <div class="footer__inner">
          <div class="footer-brand">
            <p class="footer-brand__title"><a href="<?php bloginfo( 'url' ); ?>"><?php bloginfo('name'); ?></a></p>
          </div>
          <div class="footer__sitemap">
            <section class="footer-section">
              <h2 class="footer-section__title">著者</h2>
              <ul class="footer-section__list">
                <?php
                  $query = new WP_User_Query( [
                    'role__in' => ['Author', 'Editor', 'Administrator', 'Super Admin'],
                    'count_total' => true,
                  ] );

                  $authors = $query->get_results();
                ?>
                <?php foreach ( $authors as $author ) : ?>
                  <?php
                    if ( count_user_posts( $author->ID ) < 1) {
                      continue;
                    }
                  ?>
                  <li class="footer-section__listitem">
                    <a href="<?= esc_attr( get_author_posts_url( $author->ID ) ); ?>"><?= esc_html( $author->display_name ); ?></a>
                  </li>
                <?php endforeach ?>
              </ul>
            </section>
            <section class="footer-section">
              <h2 class="footer-section__title">カテゴリ</h2>
              <ul class="footer-section__list">
                <?php
                  $categories = get_categories();
                ?>
                <?php foreach ( $categories as $category ) : ?>
                  <li class="footer-section__listitem"><a href="<?= esc_attr( get_category_link( $category->cat_ID ) ); ?>"><?= esc_html( $category->cat_name ); ?></a></li>
                <?php endforeach ?>
              </ul>
            </section>
          </div>
          <div class="footer__sitemap">
            <section class="footer-section">
              <h2 class="footer-section__title">タグ</h2>
              <ul class="footer-section__list">
                <?php
                  $tags = get_tags( [
                    'number' => 10,
                  ] );
                ?>
                <?php foreach ( $tags as $tag ) : ?>
                  <li class="footer-section__listitem _tag"><a href="<?= esc_attr( get_tag_link( $tag->term_id ) ); ?>"><?= esc_html( $tag->name ); ?></a></li>
                <?php endforeach ?>
              </ul>
            </section>
          </div>
        </div>
      </div>
      <div class="footer__copyright">
        Copyright(c) <?php bloginfo('name'); ?> All right reserved.
      </div>
    </footer>
    <?php wp_footer(); ?>
    <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
    <script async src="https://connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v3.0" id="facebook-jssdk"></script>
    <script src="https://apis.google.com/js/platform.js" async defer>{lang: 'ja'}</script>
    <script type="text/javascript" src="https://b.st-hatena.com/js/bookmark_button.js" charset="utf-8" async="async"></script>
    <script async src="https://widgets.getpocket.com/v1/j/btn.js?v=1" id="pocket-btn-js"></script>
  </body>
</html>