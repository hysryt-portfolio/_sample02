<?php get_header(); ?>

<main>
  <div class="common-breadcrumbs">
    <ul class="inner common-breadcrumbs__list">
      <li class="common-breadcrumbs__list-item"><a href="<?php bloginfo('url'); ?>">TOP</a></li>
      <?php
        $page_title = "";
        if ( is_category() || is_tag() ) {
          $page_title = single_term_title( '', false);
        } else if ( is_month() ) {
          $page_title = get_the_time('Y年m月');
        } else if ( is_search() ) {
          $search_query = get_search_query();
          $page_title = "「${search_query}」の検索結果";
        }
      ?>
      <li class="common-breadcrumbs__list-item"><?= esc_html( $page_title ); ?></li>
    </ul>
  </div>
  <div class="inner">
    <div class="common-subpage-wrapper">
      <section class="common-subpage-wrapper__main">

        <?php if ( is_category() ) : ?>
          <div class="archive-header">
            <h2 class="archive-header__title"><?= esc_html( '「' . single_cat_title( '', false ) . '」の記事一覧' ) ?></h2>
          </div>
        <?php endif ?>

        <?php if ( is_tag() ) : ?>
          <div class="archive-header">
            <h2 class="archive-header__title"><?= esc_html( '「' . single_tag_title( '', false ) . '」の記事一覧' ); ?></h2>
          </div>
        <?php endif ?>

        <?php if ( is_month() ) : ?>
          <div class="archive-header">
            <h2 class="archive-header__title"><?= esc_html( get_the_time('Y年m月') . 'の記事一覧' ); ?></h2>
          </div>
        <?php endif ?>

        <?php if ( is_search() ) : ?>
          <div class="archive-header">
            <h2 class="archive-header__title"><?= esc_html( '「' . get_search_query() . '」の検索結果' ); ?></h2>
          </div>
        <?php endif ?>

        <?php if ( is_author() && ! is_paged() ) : ?>
          <section class="archive-author">
            <h2 class="archive-author__title">著者プロフィール</h2>
            <div class="archive-author__image">
              <img src="<?= esc_attr( get_wp_user_avatar_src( get_the_author_meta('ID'), 150 ) ); ?>" alt="">
            </div>
            <div class="archive-author__info">
              <div class="archive-author__header">
                <p class="archive-author__name"><?= esc_html( get_the_author() ); ?></p>
                <ul class="archive-author__sns-list">
                  <?php if ( trim ( get_the_author_meta( 'facebook' ) ) != "" ) : ?>
                    <li class="archive-author__sns-list-item">
                      <a href="<?= esc_attr( get_the_author_meta( 'facebook' ) ); ?>"><img src="<?= get_template_directory_uri() ?>/img/common/icon_facebook.svg" alt="facebook"></a>
                    </li>
                  <?php endif ?>
                  <?php if ( trim ( get_the_author_meta( 'twitter' ) ) != "" ) : ?>
                    <li class="archive-author__sns-list-item">
                      <a href="<?= esc_attr( get_the_author_meta( 'twitter' ) ); ?>"><img src="<?= get_template_directory_uri() ?>/img/common/icon_twitter.svg" alt="twitter"></a>
                    </li>
                  <?php endif ?>
                  <?php if ( trim ( get_the_author_meta( 'instagram' ) ) != "" ) : ?>
                    <li class="archive-author__sns-list-item">
                      <a href="<?= esc_attr( get_the_author_meta( 'instagram' ) ); ?>"><img src="<?= get_template_directory_uri() ?>/img/common/icon_instagram.svg" alt="instagram"></a>
                    </li>
                  <?php endif ?>
                </ul>
              </div>
              <p class="archive-author__description">
                <?= esc_html( get_the_author_meta( 'description' ) ); ?>
              </p>
            </div>
          </section>
        <?php endif ?>

        <section class="archive-list-section">
          <?php while ( have_posts() ) : the_post(); ?>
            <article class="archive-article">
              <div class="archive-article__kv">
                <a href="<?php the_permalink(); ?>">
                  <div class="archive-article__image">
                    <img src="<?php the_post_thumbnail_url( 'eyecatch_medium' ); ?>" alt="">
                  </div>
                </a>
                <div class="archive-article__subinfo-wrapper">
                  <div class="archive-article__subinfo">
                    <a href="<?= esc_attr( get_category_posts_url() ); ?>" class="common-category-label _<?= esc_attr( get_category_slug() ); ?>"><?= esc_html( get_category_name() ); ?></a>
                    <div class="common-author">
                      <a href="<?= esc_attr( get_author_posts_url( get_the_author_meta('ID') ) ); ?>">
                        <p class="common-author__image">
                          <img src="<?= esc_attr( get_wp_user_avatar_src( get_the_author_meta('ID'), 30 ) ); ?>" alt="">
                        </p>
                        <p class="common-author__name"><span><?php the_author(); ?></span></p>
                      </a>
                    </div>
                  </div>
                </div>
              </div>
              <a href="<?php the_permalink(); ?>">
                <div class="archive-article__maininfo">
                  <p class="archive-article__date"><?= esc_html( get_post_time('Y/m/d D.') ); ?></p>
                  <h2 class="archive-article__title"><?php the_title(); ?></h2>
                  <?php
                    $tags = get_the_tags();
                  ?>
                  <ul class="archive-article__tags">
                    <?php foreach ( $tags as $tag ) : ?>
                      <li class="archive-article__tag"><?= esc_html( $tag->name ); ?></li>
                    <?php endforeach ?>
                  </ul>
                </div>
              </a>
            </article>
          <?php endwhile ?>
        </section>

        <div class="archive-pager">
          <?php
            $pages = paginate_links( [ 'type' => 'array' ] );
          ?>
          <?php if ( $pages != null ) : foreach ($pages as $pagelink) : ?>
            <?php if (strpos($pagelink, '<span') === 0) : ?>
              <p class="archive-pager__item _current"><?= $pagelink; ?></p>
            <?php else : ?>
              <p class="archive-pager__item"><?= $pagelink; ?></p>
            <?php endif ?>
          <?php endforeach; endif ?>
        </div>
      </section>
      <div class="common-subpage-wrapper__side">
        <?php get_sidebar(); ?>
      </div>
    </div>
  </div>

</main>

<?php get_footer(); ?>