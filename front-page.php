<?php get_header(); ?>

<main>
  <?php
    // 最新記事を9件取得
    $posts = query_posts([
      'posts_per_page'   => 9,
    ]);
  ?>

  <section class="top-section-latest">
    <div class="inner">
      <div class="top-section-latest__main">
        <?php if ( have_posts() ) : the_post(); ?>
          <article class="top-latest-first">
            <a href="<?php the_permalink(); ?>">
              <div class="top-latest-first__image">
                <img src="<?php the_post_thumbnail_url( 'eyecatch_medium' ); ?>" alt="">
              </div>
            </a>
            <div class="top-latest-first__info">
              <div class="top-latest-first__subinfo">
                <a href="<?= esc_attr( get_category_posts_url() ); ?>" class="common-category-label _<?= esc_attr( get_category_slug() ); ?>"><?= esc_html( get_category_name() ); ?></a>
                <div class="common-author">
                  <a href="<?= esc_attr( get_author_posts_url( get_the_author_meta('ID') ) ); ?>">
                    <p class="common-author__image">
                      <img src="<?= esc_attr( get_wp_user_avatar_src( get_the_author_meta('ID'), 30 ) ); ?>" alt="">
                    </p>
                    <p class="common-author__name"><span><?php the_author(); ?></span></p>
                  </a>
                </div>
              </div>
              <a href="<?php the_permalink(); ?>">
                <div class="top-latest-first__maininfo">
                  <p class="top-latest-first__date"><?= esc_html( get_post_time('Y/m/d D.') ); ?></p>
                  <h3 class="top-latest-first__title"><?php the_title(); ?></h3>
                </div>
              </a>
            </div>
          </article>
        <?php endif ?>
        <div class="top-section-latest__second-articles">
          <?php for ( $i = 0; $i < 2 && have_posts(); $i++ ) : the_post() ?>
            <article class="top-latest-second">
              <a href="<?php the_permalink(); ?>">
                <div class="top-latest-second__image">
                  <img src="<?php the_post_thumbnail_url( 'eyecatch_medium' ); ?>" alt="">
                </div>
              </a>
              <div class="top-latest-second__info">
                <div class="top-latest-second__subinfo">
                  <a href="<?= esc_attr( get_category_posts_url() ); ?>" class="common-category-label _<?= esc_attr( get_category_slug() ); ?>"><?= esc_html( get_category_name() ); ?></a>
                  <div class="common-author">
                    <a href="<?= esc_attr( get_author_posts_url( get_the_author_meta('ID') ) ); ?>">
                      <p class="common-author__image">
                        <img src="<?= esc_attr( get_wp_user_avatar_src( get_the_author_meta('ID'), 30 ) ); ?>" alt="">
                      </p>
                      <p class="common-author__name"><span><?php the_author(); ?></span></p>
                    </a>
                  </div>
                </div>
                <a href="<?php the_permalink(); ?>">
                  <div class="top-latest-second__maininfo">
                    <p class="top-latest-second__date"><?= esc_html( get_post_time('Y/m/d D.') ); ?></p>
                    <h3 class="top-latest-second__title"><?php the_title(); ?></h3>
                  </div>
                </a>
              </div>
            </article>
          <?php endfor ?>
        </div>
      </div>
      <div class="top-articles">
        <?php for ( $i = 0; $i < 6 && have_posts(); $i++ ) : the_post(); ?>
          <article class="top-article">
            <a href="<?php the_permalink(); ?>">
              <div class="top-article__image">
                <img src="<?php the_post_thumbnail_url( 'eyecatch_medium' ); ?>" alt="">
              </div>
            </a>
            <div class="top-article__subinfo">
              <a href="<?= esc_attr( get_category_posts_url() ); ?>" class="common-category-label _<?= esc_attr( get_category_slug() ); ?>"><?= esc_html( get_category_name() ); ?></a>
              <div class="common-author">
                <a href="<?= esc_attr( get_author_posts_url( get_the_author_meta('ID') ) ); ?>">
                  <p class="common-author__image">
                    <img src="<?= esc_attr( get_wp_user_avatar_src( get_the_author_meta('ID'), 30 ) ); ?>" alt="">
                  </p>
                  <p class="common-author__name"><span><?php the_author(); ?></span></p>
                </a>
              </div>
            </div>
            <a href="<?php the_permalink(); ?>">
              <div class="top-article__maininfo">
                <p class="top-article__date"><?= esc_html( get_post_time('Y/m/d D.') ); ?></p>
                <h3 class="top-article__title"><?php the_title(); ?></h3>
              </div>
            </a>
          </article>
        <?php endfor ?>
      </div>
      <p class="top-more-button">
        <a href="<?= esc_attr( site_url( 'archives' ) ); ?>"><span>最新記事一覧</span></a>
      </p>
    </div>
  </section>

  <?php
    // オススメタグがある記事を6件取得
    $tag_slug = 'recommend';
    $posts = query_posts([
      'posts_per_page'   => 6,
      'tag_id' => get_tags(array('slug' => $tag_slug))[0]->term_id,
    ]);
  ?>

  <section class="top-section-recommend">
    <div class="inner">
      <h2 class="top-section-recommend__title"><span>オススメ記事</span></h2>
      <div class="top-articles">
        <?php for ( $i = 0; $i < 6 && have_posts(); $i++) : the_post(); ?>
          <article class="top-article">
            <a href="<?php the_permalink(); ?>">
              <div class="top-article__image">
                <img src="<?php the_post_thumbnail_url( 'eyecatch_medium' ); ?>" alt="">
              </div>
            </a>
            <div class="top-article__subinfo">
              <a href="<?= esc_attr( get_category_posts_url() ); ?>" class="common-category-label _<?= esc_attr( get_category_slug() ); ?>"><?= esc_html( get_category_name() ); ?></a>
              <div class="common-author">
                <a href="<?= esc_attr( get_author_posts_url( get_the_author_meta('ID') ) ); ?>">
                  <p class="common-author__image">
                    <img src="<?= esc_attr( get_wp_user_avatar_src( get_the_author_meta('ID'), 30 ) ); ?>" alt="">
                  </p>
                  <p class="common-author__name"><span><?php the_author(); ?></span></p>
                </a>
              </div>
            </div>
            <a href="<?php the_permalink(); ?>">
              <div class="top-article__maininfo">
                <p class="top-article__date"><?= esc_html( get_post_time('Y/m/d D.') ); ?></p>
                <h3 class="top-article__title"><?php the_title(); ?></h3>
              </div>
            </a>
          </article>
        <?php endfor ?>
      </div>
    </div>
  </section>

  <?php
    // カテゴリー一覧を取得
    $categories = get_categories();
  ?>

  <section class="top-section-categories">
    <div class="inner">
      <div class="top-categories">
        <?php foreach( $categories as $category ) : ?>
          <section class="top-section-category">
            <h2 class="top-section-category__title"><span><?= esc_html( $category->name ); ?></span></h2>
            <div class="top-mini-articles">
              <?php
                $posts = query_posts( [
                  'cat' => $category->cat_ID,
                ] );
              ?>

              <?php for ( $i = 0; $i < 3 && have_posts(); $i++ ) : the_post(); ?>
                <article class="top-mini-article">
                  <a href="<?php the_permalink(); ?>">
                    <div class="top-mini-article__image">
                      <img src="<?php the_post_thumbnail_url( 'eyecatch_medium' ); ?>" alt="">
                    </div>
                  </a>
                  <div class="top-mini-article__maininfo">
                    <p class="top-mini-article__date"><?= esc_html( get_post_time('Y/m/d D.') ); ?></p>
                    <h3 class="top-mini-article__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <div class="top-mini-article__author">
                      <a href="<?= esc_attr( get_author_posts_url( get_the_author_meta('ID') ) ); ?>">
                        <p class="top-mini-article__author-name"><?php the_author(); ?></p>
                        <p class="top-mini-article__author-image">
                          <img src="<?= esc_attr( get_wp_user_avatar_src( get_the_author_meta('ID'), 30 ) ); ?>" alt="">
                        </p>
                      </a>
                    </div>
                  </div>
                </article>
              <?php endfor ?>
            </div>
            <p class="top-more-button">
              <a href="<?= esc_attr( get_category_posts_url() ); ?>"><span><?= esc_html( get_category_name() ); ?>の記事一覧</span></a>
            </p>
          </section>
        <?php endforeach ?>
      </div>
    </div>
  </section>

</main>

<?php get_footer(); ?>